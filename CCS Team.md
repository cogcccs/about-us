# CCS Team AoR

* Cyber Assurance
	* Policy and Standards
	* Risk Management
	* Compliance
	* Governance
* Cyber Culture & Awareness
	* Awareness and Communications
	* Training and Education
	* Phishing Simulations
	* Knowledge Continuity
* Cyber Engineering & Research
	* Security Automation
	* Threat and Research
	* Application Security
	* Vulnerability Management
* Cyber Security Operations
	* Infrastructure Security
	* Incident Response
	* Trust and Safety

  
## Way of Working

- We're a small team - there is an 80/20 rule. Whilst people are allocated/assigned to an AoR there is an obligation to provide _some_ support to other areas (20%).
- Focus should be prioritised for AoR commited activities/results. This should then move to aspirational activities/results afterwards.
- Hiring from all over Australia (Asp: work from anywhere in AU).
- Flexible working hours (6-6)
- Writing down and recording knowledge (over verbal explanations).
- Written processes (over on-the-job training).
- Public sharing of information (City-wide).
- Asynchronous communication (over synchronous communication).
- The results of work (over the hours put in).
- Defined communication channels (over informal communication channels).
- Automate work (over manually completing work).

### Engineering and Research AoR

	* Security Automation
	* Threat and Research
	* Application Security
	* Vulnerability Management

| Security Automation | Threat and Research | Application Security | Vulnerability Management |
| --- | --- | --- | --- |
| Develop and help other teams by creating tools to perform and complete security team tasks. | Create and develop intelligence data for use by security team | Provide assurance for development teams pre and post deployment | Deliver  the vulnerability program at City |
| Design, plan and build solutions as needed | Work with culture to communicate emerging and trending security events | Uplift and improve security reviews and tooling | Perform simulated exercises with SecOps and City |
| Maintain and improve solutions | Develop and maintain threat intel tools and integrations | Develop standards and controls for City | Develop and Improve initiatives to address vulnerabilities found in City |
| Scope automation activies to assist other team efforts | Develop and maintain a responsible disclosure policy | Guide, advise and facilitate application security uplifts | Develop, maintain and improve vulnerability and patch management initiatives |

### Cyber Security Operations AoR

	* Infrastructure Security
	* Incident Response
	* Trust and Safety

| Infrastructure Security | Incident Response | Trust and Safety | 
| --- | --- | --- |
| Maintain and secure the cyber security teams assets that provide services to City | Manage cyber security indients across City | Monitor and detect malicious use of City assets that target City | 
| Focus on securing City cloud and on-prem assets and infrastructure | Work with partners to ensure logging and alerting is running for our SIEM | Investigate abuse or malicious use of code on City assets, including liason with City stakeholders | 
| Propose and develop improvements to cyber security infrastructure | Triage and coordinate  incidents with communications | Liase with IESU investigations to assist in the mitigation of internal abuse |
| Develop and maintain standards and guidelines for infrastructure | Develop and improve security monitoring solutions for cyber security as well as City | Develop and define practices to make City safe from abuse | 