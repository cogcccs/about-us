# __Corporate Cyber Security Strategy__  

The City of Gold Coast’s Cyber Security Strategy directly supports the [ICT Strategic Plan 2022 – 2027](https://ecm.gccc.goldcoast.qld.gov.au/documents/A69490425), [Gold Coast’s 2022 Corporate Plan](https://www.goldcoast.qld.gov.au/files/sharedassets/public/pdfs/brochures-amp-factsheets/cogc-corporate-plan-gc-2022-rev-071218.pdf), and defines __Cities approach to managing cyber security over the next five years__. This strategy was developed to align with the [Australian Cyber Security Strategy](https://www.homeaffairs.gov.au/cyber-security-subsite/files/cyber-security-strategy-2020.pdf) and Cities commitment to manage and improve security and cyber resilience, reducing risks to business operations of the City.

City is strongly committed to cyber security and this strategy considers the evolving cyber threat landscape and needs of the City. Throughout 2020 key factors influenced our strategic direction including: 
 
__COVID-19 Response__

>The City of Gold Coast has been profoundly impacted by COVID-19 resulting in several cyber security challenges. A substantial increase in remote working and collaboration has heightened Cities cyber security risk, whilst rapid adoption of cloud services from business-led IT has presented constraints on best use of finite resources. It is crucial that IT prioritise cyber security using risk-based approaches

__Changing threat landscape__

>The rapidly evolving threat landscape is persistent, aggressive, organised and uncompromising in the targeting and impacts on City services. The City of Gold Coast as Australia’s second largest Local Government Authority has a critical role to play when it comes to cyber security. Attackers seek to harm our trusted reputation, commit financial fraud, and disrupt our rate payer services. This often frustrates our ability to respond with agility to our customer’s needs. Ransomware, phishing, and web-based attacks are major threats and pose a major risk to City and in turn our customers. A commitment is required to combat this changing threat landscape with agility.

__Countering critical infrastructure interference__

>City manages and delivers a vast environment of operational technologies that is constantly changing to meet the needs for safety of our customers. Whether it is critical infrastructure of water and wastewater services, public camera security services or the use of the internet of things to manage public safety services, City has emerging threats and risks to our OT environments that we must defend. 

__ICT Strategy__

>	The 5 year ICT Strategic Plan defines the feedback on employee and customer experiences and pain points, resulting in 3 focus areas:
>
>	1. Business Driven Outcomes: Improve Customer Experience and our Employee Experience where most of the demand has been driven from.
>
>	2. Business Demand Based ICT: aim to evolve the ICT Op Model to be flexible, responsive and meet Council’s ICT expectations.
>
>	3. Modern Enterprise Architecture Landscape: Uplift our Enterprise Information Management and to ensure that our Enterprise Foundation is suitable to build upon.


## Vision and Mission 💡

Our **vision** is to be leading example of cyber security for local government authorities.

Our **mission** is to be a Cyber Safe City, enabling the City to succeed through managing risks, fostering a cyber aware culture, and securing our services.

This will be achieved through **3 key focus areas**: 

1. **Strengthen our Cyber Culture**: Improving employee and customer experience by ensuring everyone is cognisant of the role they play in being cyber safe with a focus on:
    * Creating continuous "know-how" to stay cyber safe
    * Increasing our cyber security culture engagement
    * Driving mission of being a leading example of cyber for LGAs

2. **Cyber Security is an enabler**: Support our specialised customers and business demand based ICT with a focus on:
    * Reducing complexity of services both internally and with suppliers
    * Achieving usable and transparent security requirements
    * Reducing the time to pass security reviews
    * Achieving enterprise security certification

3. **Reduce our Threat Landscape**: Defend and protect our employees and customers with a focus on:
    * Managing and simplifying risk decision making
    * Reducing the likihood of breach
    * Improving our patch and vulnerability management
    * Developing cyber security intelligence


### Our Principles 📖

1. Defence in Depth Security: Implement layered security controls to detect, prevent, mitigate, recover in order to frustrate cyber threats. 
2. Innovative Security: security controls solutions are plug and play to be agile, quickly available to mitigate risk and to simplify complexity for City.
3. Culture First: Partnerships need to be built with industry, service providers and government to provide mutually beneficial protection against cyber threats and to improve our culture.
4. Automate Security: We focus on utilising consistent, accurate and timely deployment of controls through automated processes to reduce manual effort and error. 
5. Transparent Security: We favour writing down and recording knowledge, written processes, sharing of information and being open to feedback.
   
## Measuring Results 📈

Cyber security is a team effort. We will provide [Cyber Operational Key Results (OKRs)](https://cogcccs.atlassian.net/l/c/EhKLqz1L) on a quarterly cadence to meet our objectives over the course of this plan. We also intend to broadly increase the maturity of our cyber security capability for City to a Managed level to be assessed on an annual basis.


## Our Team 🧬

We have structured the team around four key Areas of Responsibility (AoR). Each area is responsible for driving the activities of our group.

* Cyber Assurance
	* Policy and Standards
	* Risk Management
	* Compliance
	* Governance
* Cyber Culture & Awareness
	* Awareness and Communications
	* Training and Education
	* Phishing Simulations
	* Knowledge Continuity
* Cyber Engineering & Research
	* Security Automation
	* Threat and Research
	* Application Security
	* Vulnerability Management
* Cyber Security Operations
	* Infrastructure Security
	* Incident Response
	* Trust and Safety
